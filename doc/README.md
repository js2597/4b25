## Getting Started

### 0. Set up the repository
- Ensure you have working installations of `arm-none-eabi-gcc` toolchain, `JLinkExe` and `JLinkRTTClient` from SEGGER. The default target assumes you have `make`, `cmake`, `sed`, `screen` in your $PATH.  
- Customise directory paths depending on your setup.
```bash
export ARMGCC_DIR=[arm-none-eabi] JLINK_DIR=[JLink_VER]
export JLINKPATH=$JLINK_DIR/JLinkExe
```
```bash
git clone git@gitlab.com:js2597/4b25b.git; cd 4b25b
sed -i '' -e "s|<full-path-to-warp-firmware>|.|g" tools/scripts/warp.jlink.commands
sed -i '' -e "s|<full path to JLink binary>|$JLINKPATH|g" setup.conf
sed -i '' -e "s|/usr/|$ARMGCC_DIR|g" setup.conf
```

### 1. Build the firmware
```bash
make clean; make frdmkl03
```
### 2. Load the firmware
```bash
screen -dmS load make load-warp
```
### 3. Interact with the device   
```bash
$JLINK_DIR/JLinkRTTClient
```

## Repository Structure

- [`build`](build/) stores the temporary build artifacts
- [`src/boot/ksdk1.1.0`](src/boot/ksdk1.1.0/) contains core source code of the project
- [`src/boot/ksdk1.1.0/boot.c`](src/boot/ksdk1.1.0/boot.c) contains the firwmare entry point
- [`src/boot/ksdk1.1.0/devMMA8451Q.c`](src/boot/ksdk1.1.0/devMMA8451Q.c) contains driver for the accelerometer for orientation detection
